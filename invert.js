// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

function invert(obj) {
    // validating passed data
    if ( typeof obj !== 'object') {
        return [];
    }

    let result = {};

    for ( let key in obj) {
        result[String(obj[key])] = key;
    }
    
    return result;
}

module.exports = invert;
