let mapObject = require('../mapObject.js');
let testObject = require('../objects.js');
// var _ = require('underscore');

let result = mapObject(testObject, (value, key, object) =>{
    console.log(object);
    return `key: ${key} | value: ${value} | Object: ${object}`;
});

console.log(result);

// console.log(_.mapObject(testObject, (value, key, object) => {
//     console.log(object);
//     return `key: ${key} | value: ${value} | Object: ${object}`;
// }));
