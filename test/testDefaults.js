let defaults = require('../defaults.js');
// let _ = require('underscore');
let testObject = require('../objects.js');

let result = defaults(testObject, {"name": "This will not get added"}, {"status":"This will get added"});
console.log(result);


// create some objects
// let person = {name: "John Doe", age: 20}
// let cart = {totalNumber: 10, total: 500}
// let numbers = {even: [2,4,6,8], odd: [1, 3, 5, 7]}
// let letters = {vowels: "aeiou", firstFive: "abcde"}

// console.log(defaults(person, {role: "developer"}))
// console.log(defaults(cart, {items:["apple", "brushe", "book"]}))
// console.log(defaults(letters, {vowels: "uoiea"}))

// get some properties
// console.log(person.role)      // "developer"
// console.log(cart.items)       // [ 'apple', 'brushe', 'book' ]
// console.log(numbers.prime)    // undefined
// console.log(letters.vowels)   // aeiou

// var iceCream = {flavor: "chocolate"};
// let result = defaults(iceCream);

// console.log(result);

// console.log(_.defaults(testObject,{"a":"1"}));
// var iceCream = {flavor: "chocolate"};
// console.log(_.defaults(iceCream));
