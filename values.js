// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

function values(obj) {
    // passed data is not an object
    if ( typeof obj !== 'object') {
        return [];
    }

    let result = [];

    for ( let key in obj) {
        result.push(obj[key]);
    }

    return result;
}

module.exports = values;
