// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys


function keys(obj) {
    // passed data is not an object
    if ( typeof obj !== 'object') {
        return [];
    }

    let result = [];

    for ( let key in obj) {
        result.push(String(key));
    }

    return result;
}

module.exports = keys;
