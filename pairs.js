// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj) {
    // validating passed data
    if ( typeof obj !== 'object') {
        return [];
    }

    let result = [];

    for ( let key in obj) {
        result.push([String(key), obj[key]]);
    }

    return result;
}

module.exports = pairs;
