// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

function mapObject(obj, cb) {
    // validating passed data
    if ( typeof obj !== 'object' || typeof cb !== 'function') {
        return [];
    }

    let result = {};

    for ( let key in obj) {
        result[key] = cb(obj[key], key, obj);
    }
    
    return result;
}

module.exports = mapObject;
