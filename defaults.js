// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

function defaults(obj, ...defaultProps) {
    // validating passed data
    if ( typeof obj !== 'object' || Array.isArray(defaultProps) === false) {
        return [];
    }
    
    // iterate through each object in defaultProps Array
    for ( let object of defaultProps) {
        // iterate through each key in object
        for ( let key in object) {
            // key of an object must be a string
            if ( typeof key !== 'string') {
                continue;
            }

            // key is not present in original object
            if ( key in obj === false) {
                // add key and it's value to the original object
                obj[key] = object[key];
            }
        }
    }
    
    return obj;
}

module.exports = defaults;
